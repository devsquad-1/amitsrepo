package day18;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Example65 {

	public static void main(String[] args) {

		Runnable Thread1 = () ->{
			System.out.println("This is Thread 1");
		};
		Runnable Thread2 = () ->{
			System.out.println("This is Thread 2");
		};
		Runnable Thread3 = () ->{
			System.out.println("This is Thread 3");
		};
		Runnable Thread4 = () ->{
			System.out.println("This is Thread 4");
		};
		
		ExecutorService refExecutorService = Executors.newCachedThreadPool();
		refExecutorService.execute(Thread1);
		refExecutorService.execute(Thread2);
		refExecutorService.execute(Thread3);
		refExecutorService.execute(Thread4);
	}

}
