package day18;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

//Callable thread
class CallableThread implements Callable<String> {
	@Override
	public String call() {
		return "This is CallableThread";
	}
}

//Runnable thread
class RunnableThread implements Runnable {
	@Override
	public void run() {
		System.out.println("This is RunnableThread");
	}
}

class extendThread extends Thread {
	@Override
	public void run() {
		System.out.println("This is extendThread");
	}
}

public class Example64 {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// suitable for applications that launch many short-lived tasks

		// CallableThread returns a value
		CallableThread refCallableThread = new CallableThread();

		// Return void
		RunnableThread refRunnableThread = new RunnableThread();
		extendThread refextendThread = new extendThread();

		refextendThread.start();

		Thread refThread1 = new Thread(refRunnableThread);
		refThread1.start();

		// Can execute any kind of threads
		ExecutorService executor = Executors.newCachedThreadPool();
		Future<String> future = executor.submit(refCallableThread);
		System.out.println(future.get());

		// same as Thread class but using lambda expression
		Runnable refRunnable = () -> {
			System.out.println("This is Lambda Runnable");
		};

		// Approach 1
		Thread refThread2 = new Thread(refRunnable);
		refThread2.run();

		// Approach 2
		executor.execute(refRunnable);

		// Approach 3
		executor.submit(refRunnable);

	}

}
