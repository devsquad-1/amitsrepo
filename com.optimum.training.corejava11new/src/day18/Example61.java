package day18;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// Single Thread Pool: This thread pool is used to process one task at a time. 
// In a single thread pool, we create only one thread and assign multiple tasks that will be performed sequentially.


public class Example61 {

	public static void main(String[] args) {
		
		ExecutorService refExecuterService = null;
		
		Runnable refRunnableTask1 = ()->
			System.out.println("Printing Inventory..");

		Runnable refRunnableTask2 = ()->{
			for(int i=0;i<3;i++)
				System.out.println("Printing Record.."+i);
		};
		
		try {
			refExecuterService = Executors.newSingleThreadExecutor();
			System.out.println("Start");
			refExecuterService.execute(refRunnableTask1);
			refExecuterService.execute(refRunnableTask2);
			refExecuterService.execute(refRunnableTask1);
			System.out.println("End");
		}
		catch(Exception refException){
			System.out.println("Exception Handled..");
		}
		finally {
			if(refExecuterService!=null) refExecuterService.shutdown();
			// shutdown() doesn't stop any tasks that have already been submitted
			// to the thread executer
		}
	}

}
