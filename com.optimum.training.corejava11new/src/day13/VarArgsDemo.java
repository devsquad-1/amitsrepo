package day13;

// Rule of Varargs (...)
// there can be only one variable argument(varargs) in the method
// and varargs must be the last argument (we can't add on any parameter)

// Example-48
public class VarArgsDemo {
	
	public static void test1(int data1, String... data2) {
		for (String temp : data2) {
			System.out.println(temp);
		}
	}
	
	public static void test2(int[]data3 , int... data4) { }
	 	 
	public static void test3(int data1[], int data2[]) { }
	
	public static void main(String[] args) {
		test1(10,"value1","value2","valu-n");
		String name[] = {"data-1","data-2"};
		test1(50,name);
	}
}
