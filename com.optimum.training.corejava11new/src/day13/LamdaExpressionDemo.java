package day13;

// Example-50

// Interface must be functional interface (only one abstract method)
// Lamda Expression : Argument-list, Arrow token and Body

interface Expression1{
	void getExpression1();
}

interface Expression2{
	void getExpression2(String name1);
	//void addmethod();
}

interface Expression3{
	int getExpression3(int number, int amount);
}


public class LamdaExpressionDemo {

	public static void main(String[] args) {
		
		Expression1 ref = new Expression1() {
			
			@Override
			public void getExpression1() {
				System.out.println("Hello");
				
			}
		};
		ref.getExpression1();
		
		Expression1 ref1 =()->{
			System.out.println("Hello-1");
		};

		ref1.getExpression1();
		
		Expression2 ref2 =(name)->{
			System.out.println(name);
		};
		ref2.getExpression2("calling the method..");
		
		Expression3 ref3 = (number,amount)->(number*amount);
		
		System.out.println(ref3.getExpression3(10, 20));
	}
}
