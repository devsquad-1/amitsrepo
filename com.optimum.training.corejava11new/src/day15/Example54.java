package day15;

class OCPJava11{
	int count;
	{
		System.out.println(count + "non static");	// 0
	}
	
	public OCPJava11() {
		count = 44;
		System.out.println(count + "constructor");
	}
}

public class Example54 {

	public static void main(String[] args) {
		
		var ref = new OCPJava11();
		System.out.println(ref.count);			// guess the output

	}

}
