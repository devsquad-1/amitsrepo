package pack1;

public class UserApplication1 {

	public static void main(String[] args) {
		
		User refUser = new User();
		refUser.getData1();
		refUser.getData3();
		refUser.getData4();
		
		// refUser.getData2();  compilation error, we can't access private method variable outside the class
 		
	} //  end of main()

} // end of UserApplication
