package day3;

class Employee{
	
	String employeeName="Asher";
	
	@Override
	public String toString() {
		return employeeName + " "+1000;
	}
	
} // end of Employee
 
public class Example10 {

	public static void main(String[] args) {
		
		Employee refEmployee = new Employee();
		System.out.println(refEmployee); // day3.Employee@5d22bbb7
		
	} // end of main

} // end of Example10
