package day17;

class BookTicket extends Thread{
	Object trainRef, compRef;
	BookTicket(Object trainRef, Object compRef){
		this.trainRef = trainRef;
		this.compRef = compRef;
	}
	
	@Override
	public void run() {
		synchronized (trainRef) {		// object level locking
			System.out.println("BookTicket locked on Train Object..");
		//} write line 24
			try {
				Thread.sleep(5000);
			} catch (Exception e) {
			
			  }
			System.out.println("BookTicket now waiting to lock on Compartment Object..");
		
			synchronized (compRef) {      // object level locking
				System.out.println("BookTicket locked on Compartment object..");
			} // end of synchronized (compRef)
		} // end of synchronized (trainRef)
	} // end of run()
} // end of BookTicket


class CancelTicket extends Thread{
	Object trainRef, compRef;
	CancelTicket(Object trainRef, Object compRef){
		this.trainRef = trainRef;
		this.compRef = compRef;
	}
	
	@Override
	public void run() {
		// deadlock scenario ==> compRef
		synchronized (trainRef) {		// object level locking
				System.out.println("CancelTicket locked on Train Object..");
				//} write line 50
				try {
					Thread.sleep(2000);
				} catch (Exception e) {
				
				  } 
				System.out.println("CancelTicket now waiting to lock on Compartment Object..");
			    // deadlock scenario ==> trainRef
				synchronized (compRef) {      // object level locking
					System.out.println("CancelTicket locked on Compartment object..");
				} // end of synchronized (trainRef)
				
		} // end of synchronized (compRef)
	
	} // end of run()
} // end of BookTicket


// According to best coding practice, developer should write synchronized block, not method.

public class Example59 {

	public static void main(String[] args) {
		//Object obj; // obj is pointing to null
		Object ref1 = new Object();
		Object ref2 = new Object();
		
		BookTicket refBookTicket = new BookTicket(ref1, ref2);
		CancelTicket refCancelTicket = new CancelTicket(ref1, ref2);
		
		Thread refThread1 = new Thread(refBookTicket);
		Thread refThread2 = new Thread(refCancelTicket);
		
		refThread1.start();
		refThread2.start();
			
	}
}



