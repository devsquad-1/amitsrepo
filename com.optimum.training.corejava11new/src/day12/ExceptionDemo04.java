package day12;
class Customer extends Exception{
	void myLogic() {
		try {
			checkLogin(15);
		} catch (Customer e) {
			System.out.println("Exception..");
		}
	} // myLogic()
	
	void checkLogin(int age) throws Customer {
		if (age>18) { // if its true call welcomeDashBoard()
			Customer refCustomer = new Customer();
			throw refCustomer; // throw is user define exception
		} else {
			homePage();
		}
	} // checkLogin()
	
	void homePage() {
		System.out.println("Return to HomePage..");
	} // end of homePage()
	void welcomeDashBoard() {
		System.out.println("Welcome to DashBoard..");
	} // end of welcomeDashBoard()
} // end of Customer

// Example41
public class ExceptionDemo04 {

	public static void main(String[] args) {
		
		new Customer().myLogic();

	}

}
