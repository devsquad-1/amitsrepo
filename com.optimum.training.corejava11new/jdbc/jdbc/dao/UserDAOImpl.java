package jdbc.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import jdbc.pojo.User;
import utility.DBUtility;

public class UserDAOImpl implements UserDAO{

	Connection refConnection = null;
	PreparedStatement refPreparedStatement =null;
	ResultSet refResultSet = null;
	
	CallableStatement refCallableStatement = null; // for stored procedure
	
	@Override
	public void getUserRecord() {
		
		//var sqlQuery = "select * from user";
		
		var sqlSuery = "call getUsers";
		
		try {
			refConnection = DBUtility.getConnection();
			//refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refCallableStatement = refConnection.prepareCall(sqlSuery);
			
//			refResultSet = refPreparedStatement.executeQuery();
			refResultSet = refCallableStatement.executeQuery();
				
				System.out.println("\nUser ID \t User Password");
				System.out.println("=============================");
		
				while(refResultSet.next()) {
					System.out.println(refResultSet.getInt(1) + "\t\t "+refResultSet.getString(2));
				} // end of while
		}
		catch(Exception refException) {
		
			System.out.println("Exception Handled while retrieving record.");
			
		}
		finally {
			System.out.println("\nClosing connection..");
		}
		
	}

	@Override
	public void insertRecord(User refUser) {
				
					
		// Using PreparedStatement interface
//		var sqlQuery = "insert into user(user_id,user_password) values(?,?)";

		// Using CallableStatement interface
		var sqlQuery = "{call insertRecord(?,?)}";
		
		try {
			refConnection = DBUtility.getConnection();
	
			// Using PreparedStatement interface
			
			// refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			// refPreparedStatement.setInt(1, refUser.getUserID());
			// refPreparedStatement.setString(2, refUser.getUserPassword());
	
			// Approach 1 - execute
			// refPreparedStatement.execute();  works fine
			
			// Approach 2 - executeUpdate
			// var record = refPreparedStatement.executeUpdate();
			// if(record>0) System.out.println("new record has been successfully inserted");

			
			// Using CallableStatement interface
			
			refCallableStatement = refConnection.prepareCall(sqlQuery);
			refCallableStatement.setInt(1, refUser.getUserID());
			refCallableStatement.setString(2, refUser.getUserPassword());

			refCallableStatement.execute();    
			// addBatch();
			// executeBatch();
			System.out.println("new record has been successfully inserted");
			
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while insert record.");
		}
		
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
		
	} // insertRecord

	@Override
	public void deleteRecord(User refUser) {
		
		refConnection = DBUtility.getConnection();
		
		try {
			
			var sqlQuery = "call deleteRecord(?)";
			
			//var sqlQuery = "delete from user where user_id=?";
			//refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
			//refPreparedStatement.setInt(1,refUser.getUserID()); 
			
			// executeUpdate() or execute()
			//refPreparedStatement.executeUpdate();
			
			refCallableStatement = refConnection.prepareCall(sqlQuery);
			refCallableStatement.setInt(1, refUser.getUserID());
			
			 refCallableStatement.executeUpdate();
			
			System.out.println("Record deleted Successfully..");
		}
		
		catch(Exception refException) {
			System.out.println("Exception Handled while deleteing the record..");
		}
		
		finally {
			System.out.println("Closing connection..");
		}
		
	}

	@Override
	public void updateRecord(User refUser) {
		
		refConnection = DBUtility.getConnection();
		
		try {
			
//			var sqlQuery = ("update user set user_password=? where user_id=?");
		
//			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
//			refPreparedStatement.setInt(2, refUser.getUserID());
//			refPreparedStatement.setString(1,refUser.getUserPassword());
			
//			refPreparedStatement.executeUpdate();
	
			var sqlQuery = "call updateRecord(?,?)";
			
			refCallableStatement = refConnection.prepareCall(sqlQuery);	
			
			refCallableStatement.setInt(1, refUser.getUserID());
			refCallableStatement.setString(2, refUser.getUserPassword());
			
			refCallableStatement.executeUpdate(); 
			
			
			System.out.println("Successfully Updated..");
		}
		catch(Exception refException) {
			System.out.println("Exception Handled while updating record.");
		}
		finally {
			System.out.println("Closing Connection..");
		}
	}

	@Override
	public void getUserByID(User refUser) {
		
		refConnection = DBUtility.getConnection();
		
		try {
		var sqlQuery = "call getRecordByIDnew(?,?)";	
	//	var sqlQuery = "call fetchRecordByID(?,?)"; 	// NULL
		
		refCallableStatement = refConnection.prepareCall(sqlQuery);
		refCallableStatement.setInt(1,refUser.getUserID());
	//	refCallableStatement.setString(2, refUser.getUserPassword());
	
	//	refCallableStatement.registerOutParameter(2,java.sql.Types.VARCHAR);
		
		refCallableStatement.executeQuery(); // executeUpdate() and executeQuery() also works fine
		System.out.println(refCallableStatement.getString(2));
		
		}
		catch(Exception refException) {
			System.out.println("Exception Handled while getting record by ID.");
		}
		
		finally {
			System.out.println("Closing Connection..");
		}
	}

}
